#
# Черный список WorldGuard
#
# Черный список позволяет блокировать действия, блоки и предметы от использования.
# Вы выбираете набор "предметов, влияющих на" и список "действий для выполнения."
#
###############################################################################
#
# Пример, чтобы блокировать некоторые руды от добычи и размещение:
# [coalore,goldore,ironore]
# on-break=deny,log,kick
# on-place=deny,tell
#
# События, которые можно обнаружить:
# - on-break (когда блок этого типа ломают)
# - on-destroy-with (предмет/блок, который использует пользователь, разрушая)
# - on-place (когда блок помещается)
# - on-use (когда предметы, такие как огниво или ведро используется)
# - on-interact (когда блок используется (дверь, сундук, и т.д.))
# - on-drop (когда предмет выбрасывают из инвентаря)
# - on-acquire (когда предмет входит в инвентарь игрока с помощью некоторого метода)
# - on-dispense (когда раздатчик выбрасывает предметы)
#
# Действия (для событий):
# - deny (полностью запретить, используется черным списком)
# - allow (разрешить, исползуется белым списком)
# - notify (уведомлять админов с разрешением 'worldguard.notify')
# - log (логировать в консоль/файл/базу данных)
# - tell (сказать игроку, что это не разрешено)
# - kick (кикнуть игрока)
# - ban (забанить игрока)
#
# Опции:
# - ignore-groups (Список разделенных запятыми групп, что игнорируются)
# - ignore-perms (Список разделенных запятыми разрешения, что игнорируются - создайте свои собственные разрешения!)
# - comment (Сообщение для себя, что печатаются с 'log' и 'notify')
# - message (необязательное сообщение, чтобы показать пользователю; %s это имя предмета)
#
###############################################################################
#
# Для получения дополнительной информации смотрите:
# http://wiki.sk89q.com/wiki/WorldGuard/Blacklist
#
###############################################################################
#
# Вот некоторые примеры.
# ЗАПОМНИТЕ: Если строка начинается с #, она будет проигнорирована.
#

# Запретить ведро лавы
[structure_void,barrier,bedrock,structure_block,map,command_block_minecart,repeating_command_block,chain_command_block,command_block,dragon_egg,end_crystal,bat_spawn_egg,blaze_spawn_egg,cave_spider_spawn_egg,spawn_egg,chicken_spawn_egg,cow_spawn_egg,cow_spawn_egg,donkey_spawn_egg,elder_guardian_spawn_egg,elder_guardian_spawn_egg,endermite_spawn_egg,evoker_spawn_egg,ghast_spawn_egg,guardian_spawn_egg,horse_spawn_egg,husk_spawn_egg,llama_spawn_egg,magma_cube_spawn_egg,mooshroom_spawn_egg,mule_spawn_egg,ocelot_spawn_egg,parrot_spawn_egg,pig_spawn_egg,polar_bear_spawn_egg,rabbit_spawn_egg,sheep_spawn_egg,shulker_spawn_egg,silverfish_spawn_egg,skeleton_spawn_egg,skeleton_horse_spawn_egg,slime_spawn_egg,slime_spawn_egg,squid_spawn_egg,stray_spawn_egg,vex_spawn_egg,villager_spawn_egg,villager_spawn_egg,witch_spawn_egg,wither_skeleton_spawn_egg,wolf_spawn_egg,zombie_spawn_egg,zombie_horse_spawn_egg,zombie_pigman_spawn_egg,zombie_villager_spawn_egg,fire_charge]
ignore-perms=blacklist.bypass
on-use=deny,tell
on-drop=deny,tell
on-acquire=deny,tell
on-break=deny,tell
on-place=deny,tell
on-interact=deny,tell
on-dispense=deny,tell
on-break=deny,tell
on-destroy-with=deny,tell
