# DO NOT DELETE THIS LINE
config-version: '2.0'

# ******************************************************
# * Chatty
#   Universal chat-system for Bukkit-server.
#
#   Creator: MrBrikster
#
# * Optional dependencies:
#   Vault, PlaceholderAPI, NameTagEdit.
# ******************************************************

# * GENERAL
general:
  # Plugin localization.
  #
  # en - English.
  # ru - Russian.
  # de - German.
  locale: ru

  # Chat listener priority.
  # Possible values:
  # lowest, low, normal, high, highest.
  priority: low

  # Chat logging in "plugins/Chatty/logs/".
  log: true

  # Completely cancels chat events,
  # if it cancelled by moderation method
  # (instead of clearing recipients).
  completely-cancel: false

  # Keep recipients list from previous listeners.
  # Useful if you have some plugins, that changes recipients list, and Chatty ignores it
  # (for example, Chatty may corrupt "/ignore" command of EssentialsX).
  #
  # Default: true
  keep-old-recipients: false

  # Hides vanished players from recipients (and sends "no-recipients" message when all recipients are vanished)
  # Supports EssentialsX, SuperVanish, PremiumVanish, VanishNoPacket etc.
  # Default: false
  hide-vanished-recipients: true

  # Storage player data mode.
  # true - data will store by UUIDs.
  # false - data will store by nicknames.
  uuid: true

  # Enables support for "-3" chat range.
  # Messages from that chats will be sent to all BungeeCord servers with Chatty.
  #
  # Notes:
  # * Console logs will not prints on empty BungeeCord servers (non-fixable)
  # * So Chatty send message to another server, there must be chat with the same name and range -3.
  bungeecord: false

# * CHATS
#
# On the server there can be different chats with different settings,
# whether it's donate-chat, admin-chat, or the usual local and global.
#
# Permissions:
# chatty.chat.<chat> - permission for both writing and seeing messages.
# chatty.chat.<chat>.write - permission for writing messages.
# chatty.chat.<chat>.see - permission for seeing messages.
#
# Chat has the following options:
# *enable*       - on/off chat.
# *display-name* - name of chat, displaying in placeholders and locale messages. Default: regular name of chat.
# *format*       - chat format string.
# *range*        - chat range. -1 to world-wide chat, -2 to server-wide, -3 to BungeeCord-wide chat.
# *symbol*       - character from which message should begin.
# *cooldown*     - message cooldown in seconds (bypass permission - chatty.cooldown.<chat>).
# *money*        - message price (depends on Vault).
# *permission*   - check permissions for this chat? Default: true.
# *command*      - Command that is used to switch to chat to use it by default if symbol is not specified. Default: empty.
# *aliases*      - Aliases for command. Default: empty.
chats:
  local:
    enable: true
    display-name: 'Local'
    format: '&3ʟ | &r{prefix}{player}{suffix} &b➦ &f{message}'
    range: 200
    cooldown: 1
    command: 'localchat'
    aliases: ['lch']

  global:
    enable: true
    display-name: 'Global'
    format: '&bɢ | &r{prefix}{player} &b➦ &f{message}'
    range: -1
    symbol: '!'
    cooldown: 3
    command: 'globalchat'
    aliases: ['gch']

  # Example chat with multiline format
  notify:
    enable: true
    format: |
      &b==========================================
        &c{player}&8: &e{message}
      &b==========================================
    range: -3
    symbol: 'Ⓖ'
    cooldown: -1

    # Plays sound for all chat recipients
    sound: CLICK

    # Disables some moderation methods for this chat
    moderation:
      caps: false
      swear: false
      advertisement: false
      unicode: false

    # Disables spy-mode for this chat
    spy: false

# * PRIVATE MESSAGING
pm:
  # Enables private messaging function.
  enable: true

  # Allows console to messaging players
  # and messaging players to console.
  # Default: false
  allow-console: true

  # Allows to PM players, that in vanish
  # Supports EssentialsX, SuperVanish, PremiumVanish, VanishNoPacket etc.
  # Default: true
  allow-pm-vanished: false

  # Format of messages
  #
  # Variables:
  # {sender-name}, {recipient-name} - nicknames of sender and recipient.
  # {sender-prefix}, {recipient-prefix} - prefixes of sender and recipient.
  # {sender-suffix}, {recipient-suffix} - suffixes of sender and recipient.
  # {message} - private message.
  format:
    recipient: '&cPM | &7{sender-prefix}{sender-name}{sender-suffix} &6-> &7{recipient-prefix}{recipient-name}: &f{message}'
    sender: '&cPM | &7{sender-prefix}{sender-name}{sender-suffix} &6-> &7{recipient-prefix}{recipient-name}: &f{message}'

  commands:
    msg:
      # Enables "/msg" command.
      # Permission: chatty.command.msg
      enable: true
      # Aliases for "/msg" command.
      aliases: [ 'message', 'pm', 'm', 'whisper', 'w', 'tell', 't' ]

    # To keep compatibility with EssentialsX
    # main name of command is "/r".
    reply:
      # Enables "/r" command.
      # Permission: chatty.command.reply
      enable: true
      # Aliases for "/r" command.
      aliases: [ 'reply' ]

    ignore:
      # Enables "/ignore" command.
      # Permission: chatty.command.ignore
      enable: true
      # Aliases for "/ignore" command.
      aliases: [ ]

# * SPY
#
# Mode for spying for chat and private messages.
spy:
  enable: true

  # Spy-mode message format.
  format:
    # Permission: chatty.spy.<chat>
    chat: '&8[Spy] &r{format}'

    # Permission: chatty.spy.pm
    #
    # Uses sender format of PM.
    # Supports variables:
    # {format}, {sender-prefix}, {sender-name}, {sender-suffix},
    # {recipient-prefix}, {recipient-name}, {recipient-suffix}, {message}
    pm: '&8[Spy] &r{format}'

# * JSON
#
# Chat formatting with additional interactive features.
#
# 1.7.10 and newer.
json:
  enable: true

  # Command or text that executes (by player) when you click on the player's name.
  #
  # *** You can use EITHER "command" OR "suggest" OR "link".
  #
  # Supports PlaceholderAPI.
  # You can use "{player}" variable here.
  command: /msg {player}

  # Command or text that suggests when you click on the player's name.
  #
  # *** You can use EITHER "command" OR "suggest" OR "link".
  #
  # Supports PlaceholderAPI.
  # You can use "{player}" variable here.
  suggest: '/msg {player} '

  # URL that opens when when you click on the player's name.
  #
  # You must use "http://" or "https://" prefix, else you will crash Minecraft client :D
  #
  # *** You can use EITHER "command" OR "suggest" OR "link".
  #
  # Supports PlaceholderAPI.
  # You can use "{player}" variable here.
  link: 'https://infinity.momc.shop'

  # Hover tooltip for the player's name.
  # Supports PlaceholderAPI.
  #
  # You can use "{player}" variable here.
  tooltip:
    - '&fГильдия: &b%tab_replace_guilds_name%'
    - '&fБаланс: &b%vault_eco_balance_commas%'

  # Mentions.
  # "@nickname" will notify player that he's mentioned.
  # Also it will replaced to a clickable JSON-part.
  #
  # Requires permission: chatty.mentions
  mentions:
    enable: true

    # Display format of mention.
    format: '&6@{player}'

    # Plays sound to player, that was mentioned.
    # Remove the line if not needed.
    sound: CLICK

    # Allows to mention vanished players
    # Default: true
    allow-vanished: false

    # Command or text that executes (by player) when you click on the mention.
    #
    # *** You can use EITHER "command" OR "suggest" OR "link".
    #
    # Supports PlaceholderAPI.
    # You can use "{player}" variable here.
    command: '@{player}'

    # Command or text that suggests when you click on the mention.
    #
    # *** You can use EITHER "command" OR "suggest" OR "link".
    #
    # Supports PlaceholderAPI.
    # You can use "{player}" variable here.
    suggest: '/msg {player} '

    # URL that opens when when you click on the mention.
    #
    # You must use "http://" or "https://" prefix, else you will crash Minecraft client :D
    #
    # *** You can use EITHER "command" OR "suggest" OR "link".
    #
    # Supports PlaceholderAPI.
    # You can use "{player}" variable here.
    link: ''

    # Hover tooltip for the mention.
    # Supports PlaceholderAPI.
    #
    # You can use "{player}" variable here.
    tooltip:
      - '&fНажмите сюда чтобы написать игроку &6{player}'

  # Swears hovers.
  #
  # Available properties:
  # tooltip, suggest
  #
  # Permission: chatty.swears.see
  swears:
    enable: true
    tooltip:
      - '&o{word}'
      - ''
      - '&bНажмите здесь, чтобы добавить слово в белый список.'
    suggest: /swears add {word}

  # Replacements for chat formats.
  #
  # You can replace some plain text with clickable parts
  # to make chat more interactive.
  #
  # Available properties:
  # text, tooltip, command, suggest, link.
  replacements:
    'somegroup':
      # Original text.
      original: '&cSomegroup'

      # New text.
      #
      # Supports PlaceholderAPI.
      # You can use "{player}" variable here.
      #
      # Remove this line, if you need original text.
      text: '&cSomegroup'

      # Hover tooltip for the text.
      #
      # Supports PlaceholderAPI.
      #
      # You can use "{player}" variable here.
      tooltip:
        - '&cThis is admin.'
        - '&4&lBe afraid of him!'
        - ''
        - '&eClick here to see the server rules.'

      # *** You should use "command" OR "suggest-command" OR "link" and cannot combine.
      # Command that executes when you click on the text.
      #
      # Supports PlaceholderAPI.
      #
      # You can use "{player}" variable here.
      command: /rules

# * NOTIFICATIONS
#
# There are four types of notifications:
# via Chat, Title (1.8.8 and newer), ActionBar (1.8.8 and newer) and Advancements (1.12 and newer).
notifications:
  # Chat notifications.
  chat:
    # On/off chat notifications.
    enable: true

    # Notification lists.
    lists:
      # List "default".
      # Permission: chatty.notification.chat.default
      default:
        # Messages interval.
        time: 120

        # Messages prefix.
        prefix: ''

        # List of messages.
        # Supports PlaceholderAPI.
        # Supports JSON formatting.
        #
        # Notification messages are using "/tellraw" JSON format.
        # For example: {"text":"","extra":[{"text":"Try to search it with ","color":"white"},{"text":"search system","color":"yellow","clickEvent":{"action":"open_url","value":"http://google.com"},"hoverEvent":{"action":"show_text","value":"Search it in Google"}}]}
        # This JSON will be a message "Try to search it with search system" with hover and clickable text
        messages:
          - '\n&b&lОПОВЕЩЕНИЕ &8|| &7Твой пароль менее 10 символов? Поменяй его: &b/changepassword\n '
          - '\n&b&lОПОВЕЩЕНИЕ &8|| &7Администрация желает тебе много &bдрузей &7и &bалмазов &7=)\n '
          - '\n&d&lПОСПЕШИ &8|| &7Для тебя доступен специальный набор предметов - &d/kit top\n '
          - '\n&b&lОПОВЕЩЕНИЕ &8|| &7Не добавляй в приват того, в ком не уверен.\n '
          - '\n&b&lОПОВЕЩЕНИЕ &8|| &7Каждые 10м ты можешь дюпнуть вещи на озере - &b/warp hole.\n '
          - '\n&b&lОПОВЕЩЕНИЕ &8|| &7Чаще меняй пароль ради &bбезопасности&7!\n '
          - '\n&r &b■ &fДобавь наш сервер в список серверов&r\n&r &b■ &fЧтобы не потерять нас!\n &b■ &fНаш IP: &bmonocraft.ru\n\n&r'

        # Permission for seeing notification.
        # Default: true
        permission: false

        # Show messages in random order.
        # Default: false
        random: true
      # List "donate".
      # Permission: chatty.notification.chat.default
      donate:
        time: 180
        prefix: ''

        # List of messages.
        #
        # Notification messages are using "/tellraw" JSON format.
        # For example: {"text":"","extra":[{"text":"Try to search it with ","color":"white"},{"text":"search system","color":"yellow","clickEvent":{"action":"open_url","value":"http://google.com"},"hoverEvent":{"action":"show_text","value":"Search it in Google"}}]}
        # This JSON will be a message "Try to search it with search system" with hover and clickable text
        messages:
          - '\n&r &b■ &fСегодня скидки &bтолько &n1 день&r\n&r &b■ &fРазмер скидки - до &c&l90%\n &b■ Успей купить донат, &bпока есть скидки\n\n &b■ &fСайт для покупки: &b&lwww.infinity.momc.shop\n\n&r'
          - '\n&r &c■ &fТолько&c сегодня&f скидки на &c&n1 день!&r\n&r &c■ &e&lУвеличены &fдо 90%, до завтрашнего дня\n &c■ Успей купить, &cпо минимальным ценам\n\n &c■ &fСайт для покупки привилегии: &c&lwww.infinity.momc.shop\n\n&r'
          - '\n&r &7■ &fХочешь показать кто тут &7&lглавный&f?&r\n&r &7■ &fПокупай привилегию &8&lWITHER &fвсего за &c&m2990&r &b299 рублей\n &7■ &fУ него есть возможность использовать команду &7&l/vanish&f!\n\n &7■ &fПокупай на нашем сайте: &7&lwww.infinity.momc.shop\n\n&r'
          - '\n&r &c■ &fХочешь разрушать &c&lчужие регионы?&f?&r\n&r &c■ &fПокупай привилегию &c&lSPONSOR &fвсего за &c&m24900&r &b2490 рублей\n &c■ &fУ него есть доступ ко &cВСЕМ&f регионам игроков&f!\n\n &c■ &fПокупай на нашем сайте: &c&lwww.infinity.momc.shop\n\n&r'
          - '\n&r &b■ &fХочешь быть в курсе &bвсех новостей проекта?&r\n&r &b■ &fПодписывайтесь на наше сообщетсво в &b&lВК\n &b■ &fИ заходите на наш сервер &3&lDiscord\n &b■ &fГруппа &b&lВК&3: vk.com/monocraft\n &b■ &fСервер &3&lDiscord&3: https://discord.gg/DRJpBWP\n\n&r'
          - '\n&r &b■ &fХочешь купить донат, &bно сомневаешься?&r\n&r &b■ &fНе бойся. Мы &b&nне обманываем&f своих игроков!\n &b■ &fКоличество успешных покупок это подтверждает.\n\n &b■ &fПокупай на нашем сайте: &b&lwwww.infinity.momc.shop\n\n&r'
          - '\n&r &e■ &fТолько сегодня на сервере скидки до &e90%&r\n&r &e■ &fУспей купить &eCREATIVE&f всего за &c&m290&r &b29 рублей\n &e■ &fУ него есть доступ на команду &e/gm 1\n\n &e■ &fПокупай на нашем сайте: &e&lwwww.infinity.momc.shop\n\n&r'
          - '\n&r &b■ &fНе знаешь какие возможности ты упускаешь?&r\n&r &b■ &fПиши команду &b/donate &fи развлекайся!\n\n &b■ &fПокупай на нашем сайте: &b&lwwww.infinity.momc.shop\n\n&r'
          - '\n&r &b■ &fАдекватные &bцены&f на донат?&r\n&r &b■ &fЭто то - что мы можем предложить!\n &b■ &fПривилегия &9VIP &fвсего за &c&m90&r &b9 рублей\n\n &b■ &fПокупай на нашем сайте: &b&lwwww.infinity.momc.shop\n\n&r'
          - '\n&r &b■ &fВесь донат на сайте покупается навсегда!&r\n&r &b■ &fПокупай донат со скидкой до &b90%\n &b■ &fНа нашем сайте: &b&lwwww.infinity.momc.shop\n\n&r'
        permission: false
        random: true

  # ActionBar notifications.
  # It's static notifications,
  # which changes the message with interval.
  #
  # Permission: chatty.notification.actionbar
  actionbar:
    # On/off ActionBar notifications.
    # Only 1.8+.
    enable: false

    # One message time.
    time: 60

    # Messages prefix.
    prefix: ''

    # List of messages.
    # Supports PlaceholderAPI.
    messages:
      - '&cThe server is using Chatty!'
      - '&bThe server is using Chatty!'

    # Permission for seeing notification.
    # Default: true
    permission: false

    # Show messages in random order.
    # Default: false
    random: true

  # Title notifications.
  title:
    # On/off title notifications.
    enable: false

    # Notification lists.
    lists:
      # List "default".
      # Permission: chatty.notification.title.default
      default:
        # Messages interval.
        time: 60

        # List of messages.
        # Supports PlaceholderAPI.
        messages:
          - |
            &cTitle
            &eTitle
          - '&bTitle!'

        # Permission for seeing notification.
        # Default: true
        permission: false

        # Show messages in random order.
        # Default: false
        random: false

  # Advancements (Achievements) notifications.
  advancements:
    # Advancements notifications.
    # Only 1.12 and newer.
    enable: true
    # Notification lists.
    lists:
      # List "default".
      # Permission: chatty.notification.advancements.default
      default:
        # Messages interval.
        time: 120

        # List of messages.
        messages:
          - icon: 'minecraft:diamond'
            header: '&bСкидки'
            footer: '&eСкидки на донат до 90%'

        # Permission for seeing notification.
        # Default: true
        permission: false

        # Show messages in random order.
        # Default: false
        random: false

# * MODERATION
#
# Automatic chat moderation tools.
# Works with private messages.
moderation:
  caps:
    # On/off caps protection.
    # Bypass permission: chatty.moderation.caps
    enable: true

    # Minimal length of message to caps checking.
    length: 6

    # Minimal upper case percent for caps.
    percent: 65

    # On/off caps message blocking.
    # When false, message will be sent in lower case.
    block: true

  # On/off unicode filter
  unicode:
    enable: true
    replacement: 'я чмо'

  # On/off playtime filter, time in seconds
  playtime:
    enable: true
    time: 3600

  advertisement:
    # On/off advertisement protection.
    # Bypass permission: chatty.moderation.advertisement
    enable: true

    # Regular expressions for IP-addresses and websites.
    patterns:
      ip: '\b((\d{1,2}|2(5[0-5]|[0-4]\d))[._,)(-]+){3}(\d{1,2}|2(5[0-5]|[0-4]\d))(:\d{2,7})?'
      web: '((?i)\b(https?:\/\/)?[\w\.а-яА-Я-]+\.([a-z]{2,4}|[рР][фФ]|[уУ][кК][рР])\b(:\d{2,7})?(\/\S+)?)|(w\s*i\s*n\s*m\s*\s*c)'

    # On/off advertisement message blocking.
    # When false, advertisement will be replaced.
    block: true

    # Replacement for advertisement.
    replacement: '<реклама>'

    # Whitelisted IP-addresses and sites.
    whitelist:
      - 'example.com'
      - 'vk.com'
      - 'google.com'
      - 'youtube.com'
      - 'vk.com/monocraft'
      - 'monocraft.ru'
      - 'infinity.momc.shop'
      - 'yandex.ru'
      - '192.168.1.1'
      - '127.0.0.1'

  swear:
    # On/off swear protection.
    # Bypass permission: chatty.moderation.swear
    enable: false

    # On/off swear message blocking.
    # When false, swears will be replaced.
    block: true

    # Replacement for swears.
    replacement: '<мат>'
    # Files with swears are located at "Chatty/swears"

# * MISCELLANEOUS
miscellaneous:
  commands:
    # Used to switch default chat (when symbol is not specified): /chat <chat>
    # Permission: chatty.command.chat
    chat:
      enable: true
      aliases: [ 'chats', 'switchchat' ]

    # Clear chat for yourself: /clearchat
    # Permission: chatty.command.clearchat
    #
    # Clear chat for all: /clearchat all
    # Permission: chatty.command.clearchat.all
    clearchat:
      enable: true
      aliases: [ 'chatclear' ]

    # Change chat (and NametagEdit, if enabled) prefix
    # /prefix <player> <prefix>
    #
    # Permissions:
    # chatty.command.prefix
    # chatty.command.prefix.others
    prefix:
      enable: true

      # Automatically appends a substring after prefix.
      after-prefix: ' '

      # Limits prefix length (inclusive "after-prefix")
      length-limit:
        min: 3
        max: 16

      # Auto-applying in NametagEdit.
      # (Requires NameTagEdit plugin).
      auto-nte: false

    # Change chat (and NametagEdit, if enabled) suffix
    # /suffix <player> <suffix>
    #
    # Permissions:
    # chatty.command.suffix
    # chatty.command.suffix.others
    suffix:
      enable: true

      # Automatically appends a substring before suffix.
      before-suffix: ' '

      # Limits suffix length (inclusive "before-suffix")
      length-limit:
        min: 3
        max: 16

      # Auto-applying in NametagEdit.
      # (Requires NameTagEdit plugin).
      auto-nte: false

  # Change vanilla Minecraft messages.
  # Supports PlaceholderAPI.
  vanilla:
    join:
      enable: true

      # Set to '' if you want to make it hidden.
      message: '&b► &e{prefix}{player}{suffix} &bзашёл на сервер!'

      # Plays sound to all, when player joins.
      # Remove the line if not needed.
      sound: ORB_PICKUP

      # Permission: chatty.misc.joinmessage
      # Default: true
      permission: true

      # Specified parameters for the first join
      first-join:
        # First join message.
        # Set to '' if you don't want to see special message for the first join.
        message: '&b► &e{prefix}{player}{suffix} &bзашёл на сервер!'
        # Plays sound to all, when player joins.
        # Remove the line if not needed.
        sound: ORB_PICKUP

    quit:
      enable: true

      # Set to '' if you want to make it hidden.
      message: '&e► &e{prefix}{player}{suffix} &eвышел с сервера.'

      # Plays sound to all, when player quits.
      # Remove the line if not needed.
      sound: ORB_PICKUP

      # Permission: chatty.misc.quitmessage
      # Default: true
      permission: true

    death:
      enable: true

      # Set to '' if you want to make it hidden.
      message: '&c► &e{prefix}{player}{suffix} &cумер.'

      # Permission: chatty.misc.deathmessage
      # Default: true
      permission: false
